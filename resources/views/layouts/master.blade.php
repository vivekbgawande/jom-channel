@include('layouts.navbar')

<div class="wrapper">
   
    <div class="main">
        <div class="section section-basic" id="basic-elements">
            @yield('content')
        </div>
    </div>
</div>



    
@include('layouts.footer')